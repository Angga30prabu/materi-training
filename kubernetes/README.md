# Setup Kubernetes Cluster #

[![Konsep Kubernetes](img/09-istilah-dalam-kubernetes.jpg)](img/09-istilah-dalam-kubernetes.jpg)

1. Setup Kubernetes Cluster di provider yang dipilih:

    * Digital Ocean
    * Google
    * Amazon
    * Azure

    Setelah mendapatkan konfigurasi, set ke environment variable supaya tidak perlu menyebutkan di tiap pemanggilan

    ```
    export KUBECONFIG=~/.kube/belajar-k8s-kubeconfig.yaml
    ```

    Tanpa environment variable, tiap pemanggilan harus menyebutkan nama filenya seperti ini

    ```
    kubectl --kubeconfig /Users/endymuhardin/.kube/belajar-k8s-kubeconfig.yaml get nodes
    ```

2. Cek node dalam cluster

    ```
    kubectl get nodes
    ```

    Outputnya seperti ini

    ```
    NAME                  STATUS   ROLES    AGE    VERSION
    pool-lk6nbmy8j-wwq5   Ready    <none>   114m   v1.15.3
    pool-lk6nbmy8j-wwqk   Ready    <none>   114m   v1.15.3
    pool-lk6nbmy8j-wwqs   Ready    <none>   114m   v1.15.3
    ```

3. Deploy persistent volume

    Cek dulu kondisi awal persistent volume dalam cluster

    ```
    kubectl get pv
    ```

    Outputnya seperti ini, belum ada PV

    ```
    No resources found in default namespace.
    ```

    Sekarang, kita deploy persistent volume

    ```
    kubectl apply -f 01-volume.yml
    ```

    Hasil pengecekan setelah deployment

    ```
    NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                  STORAGECLASS       REASON   AGE
pvc-1ffcfb14-2feb-4519-b11a-10697ed0eda3   5Gi        RWO            Delete           Bound    default/bukutamu-pvc   do-block-storage            11s
    ```

4. Cek persistent volume

5. Deploy database pod dan service

6. Baca log deployment database

7. Cek service database

7. Deploy aplikasi web pod dan service

8. Baca log deployment aplikasi

9. Cek service aplikasi

10. Browse aplikasi

11. Undeploy pod dan service